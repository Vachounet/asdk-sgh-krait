#!/bin/bash
#These are definitions to enable shortcoding commands which makes the script look cleaner
#Check out the folder at the end of the directory paths, make your own but keep files in these directories

DEFCONFIG=cyanogen_d2att_defconfig
MAKE="make CONFIG_NO_ERROR_ON_MISMATCH=y -j4"
export KERNELDIR=/home/ayysir/android/kernel/android_kernel_samsung_d2
export INITRAMFS_DEST=$KERNELDIR/kernel/usr/initramfs
export PACKAGEDIR=/home/ayysir/android/kernel/kernelwork/asdk_out
export INITRAMFS_SOURCE=/home/ayysir/android/kernel/Ramdisks
export Meta=/home/ayysir/android/kernel/Ayysir/AOSP_4.2/META-INF
export Drivers=/home/ayysir/android/kernel/Ayysir/AOSP_4.2/Drivers
export extras=/home/ayysir/android/kernel/Ayysir/AOSP_4.2/updated_modules
export System=/home/ayysir/android/kernel/Ayysir/AOSP_4.2/system
export Data=/home/ayysir/android/kernel/Ayysir/AOSP_4.2/data
export USE_SEC_FIPS_MODE=true
export USE_CCACHE=1
export CCACHE_DIR=/home/ayysir/.ccache
export ARCH=arm
export CROSS_COMPILE=/home/ayysir/android/kernel/toolchains/android-toolchain-4.7.3/bin/arm-eabi-

# Colorize and add text parameters
red=$(tput setaf 1) # red
grn=$(tput setaf 2) # green
cya=$(tput setaf 6) # cyan
txtbld=$(tput bold) # Bold
bldred=${txtbld}$(tput setaf 1) # red
bldgrn=${txtbld}$(tput setaf 2) # green
bldblu=${txtbld}$(tput setaf 4) # blue
bldcya=${txtbld}$(tput setaf 6) # cyan
txtrst=$(tput sgr0) # Reset

echo -e "${bldred} Set CCACHE ${txtrst}"
ccache -M50

echo ""

echo -e "${bldcya} Switch to JellyBean AOSP 4.2${txtrst}"
cd $KERNELDIR 
git checkout cm-10.1

echo ""

echo -e "${bldred} Remove old Package Files ${txtrst}"
rm -rf $PACKAGEDIR/*

echo ""

echo -e "${bldred} Setup Out Directories ${txtrst}"
mkdir -p $PACKAGEDIR/system/lib/modules

echo ""

echo -e "${bldred} Remove old zImage ${txtrst}"
rm $PACKAGEDIR/zImage
rm arch/arm/boot/zImage

echo ""

echo -e "${bldred} Remove old Ramdisk ${txtrst}"
rm $INITRAMFS_SOURCE/ramdisk.krait.gz

echo ""

echo -e "${bldred} Removing pesky backup files ${txtrst}"
cd ~/android/kernel
find ./ -name '*~' | xargs rm

echo ""

echo -e "${bldred} Use Defconfig Settings ${txtrst}"
cd $KERNELDIR
make $DEFCONFIG
make clean

echo ""

echo -e "${bldred} Compiling.. ${txtrst}"
script -q ~/Compile.log -c "
$MAKE"

echo ""

if [ -e $KERNELDIR/arch/arm/boot/zImage ]; then
	echo -e "${bldred} Copy modules to Package ${txtrst}"
	cp -a $(find . -name *.ko -print |grep -v initramfs) $PACKAGEDIR/system/lib/modules/
 	
	echo ""

	echo -e "${bldred} Copy zImage to Package ${txtrst}"
	cp $KERNELDIR/arch/arm/boot/zImage $PACKAGEDIR/zImage
 	
	echo ""

	echo -e "${bldred} Ramdisk Readying.. ${txtrst}"
	cp $KERNELDIR/mkbootfs $INITRAMFS_SOURCE
	cd $INITRAMFS_SOURCE/
	./mkbootfs ASDK-krait | gzip > ramdisk.krait.gz
	rm mkbootfs
 	
	echo ""

	echo -e "${bldred} Making Boot.img.. ${txtrst}"
	cp $KERNELDIR/mkbootimg $PACKAGEDIR
	cp $INITRAMFS_SOURCE/ramdisk.krait.gz $PACKAGEDIR
	cd $PACKAGEDIR
	./mkbootimg --cmdline 'console = null androidboot.hardware=qcom user_debug=31 zcache' --kernel $PACKAGEDIR/zImage --ramdisk $PACKAGEDIR/ramdisk.krait.gz --base 0x80200000 --pagesize 2048 --ramdiskaddr 0x81500000 --output $PACKAGEDIR/boot.img
	rm mkbootimg
 	
	echo ""

	echo -e "${bldred} Extra Important files ${txtrst}"	
	cp -R $Meta $PACKAGEDIR
	cp -R $System/* $PACKAGEDIR/system
#	rm $PACKAGEDIR/system/lib/modules/*
#	cp $extras/* $PACKAGEDIR/system/lib/modules
	cp $Drivers/* $PACKAGEDIR/system/lib/
 	
	echo ""

	export curdate=`date "+%m-%d-%Y"`
        cp ~/Compile.log ~/android/compile_logs/Success-Krait-$curdate.log
        rm ~/Compile.log
	cd $PACKAGEDIR
	rm ramdisk.krait.gz
	rm zImage
	rm ../../ASDK-AOSP-3.4.x-*.zip
	rm ../ramdisk.krait.gz
	rm ../zImage
	zip -r ../ASDK-AOSP-3.4.x-$curdate-EXP.zip .
	mv ../ASDK-AOSP-3.4.x-$curdate-EXP.zip ~/android/kernel

	echo "ASDK HOT OFF THE PRESS"

else
	echo "KERNEL DID NOT BUILD! no zImage exist"
	export curdate=`date "+%m-%d-%Y"`
	cp ~/Compile.log ~/android/compile_logs/Failed-Krait-$curdate.log
fi;

exit 0

